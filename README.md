Basic information
----

#### Testing
To run unit tests enter `python manage.py test`

#### Running

To run development server at port XXXX enter `python manage.py runserver XXXX`

#### Project structure
`\uploads` - uploaded files go here

`\templates` - django templates for rendering

`\apps\benford_law` - django app to tackle benford's law problem

`\CodePoetsTask` - django internal configuration (no code concerning the given task resides here)

#### Note

In production environment `Dockerfile` should be done with caching mechanism and multistage build for security reasons