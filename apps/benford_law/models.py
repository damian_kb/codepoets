from django.db.models import Model, FileField, DateTimeField


class DataSet(Model):
    file = FileField(upload_to='uploads/')
    upload_time = DateTimeField(auto_now_add=True,blank=True)