import codecs
import csv
import math
from collections import Counter

CHI_SQUARE_5_CRITCAL_VALUE = 16.919
CHI_SQUARE_1_CRITCAL_VALUE = 21.666
DATA_COLUMN_INDEX = 2


def generate_benford_probabilities():
    digits = list(range(1, 10))
    return list(math.log((x + 1) / x, 10) for x in digits)


def calculate_probabilities(data_set):
    counts = Counter(data_set)
    return list(x / len(data_set) for x in counts.values())


def test_chi_square_for_benford(observations):
    size = len(observations)
    expected_probabilities = generate_benford_probabilities()
    counts = Counter(observations)
    chi_square = sum(((observed - expected * size) ** 2) / (expected * size) for expected, observed in
                     zip(expected_probabilities, counts.values()))

    return CHI_SQUARE_1_CRITCAL_VALUE > chi_square, CHI_SQUARE_5_CRITCAL_VALUE > chi_square


def check_file_conform_to_data_format(file, decode=False):
    sample = file.read(1024)
    if decode:
        sample = sample.decode()
        input = codecs.iterdecode(file, 'utf-8')
    else:
        input = file

    # Set dialect and read data
    dialect = csv.Sniffer().sniff(sample, delimiters=';,\t')
    reader = csv.reader(input, dialect)

    # Detect column names
    file.seek(0)
    columns = reader.__next__()
    for idx, row in enumerate(reader):
        value = row[DATA_COLUMN_INDEX][0]
        if not value.isnumeric():
            raise ValueError(
                f"Malformed data set! Line:{idx + 1} Column:{DATA_COLUMN_INDEX + 1} Value:{row[DATA_COLUMN_INDEX]}")


def extract_data_set_from_file(file):
    dialect = csv.Sniffer().sniff(file.read(1024), delimiters=';,\t')
    file.seek(0)
    # Set dialect and read data
    reader = csv.reader(file, dialect)
    # Detect column names
    columns = reader.__next__()
    data_set = []
    for row in reader:
        data_set.append(str(row[DATA_COLUMN_INDEX][0]))

    return data_set
