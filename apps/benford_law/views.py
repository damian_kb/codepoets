import json

from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from django.views import View
from django.views.generic import ListView
from plotly.graph_objs import Bar
from plotly.offline import plot

from apps.benford_law.forms import DataSetForm
from apps.benford_law.logic import generate_benford_probabilities, calculate_probabilities, test_chi_square_for_benford, \
    extract_data_set_from_file, check_file_conform_to_data_format
from apps.benford_law.models import DataSet


class DataSetView(ListView):
    model = DataSet
    paginate_by = 100  # if pagination is desired


class BenfordLawView(View):
    def get(self, request, data_set_id):
        data_set = DataSet.objects.get(pk=data_set_id)
        data_set.file.open('r')

        data_set = extract_data_set_from_file(data_set.file)

        expected_probabilities = generate_benford_probabilities()
        observed_probabilities = calculate_probabilities(data_set)
        chi_square_results = test_chi_square_for_benford(data_set)
        digits = list(range(1, 10))

        plot_div = plot([Bar(x=digits, y=expected_probabilities, name='expected', opacity=0.8),
                         Bar(x=digits, y=observed_probabilities, name='observed', opacity=0.8)],
                        output_type='div')

        return render(request, 'benford_law/chart.html', {'plot_div': plot_div,
                                              'chi_square_alfa_1': chi_square_results[0],
                                              'chi_square_alfa_5': chi_square_results[1]})


def upload_file(request):
    if request.method == 'POST':
        form = DataSetForm(request.POST, request.FILES)
        if form.is_valid():
            instance = form.save()
            instance.file.close()
            return HttpResponseRedirect(redirect_to=reverse("benford-law", args=[instance.id]))

        else:
            return HttpResponse(status=400,content=json.dumps(form.errors))
    else:
        form = DataSetForm()
    return render(request, 'benford_law/upload.html', {'form': form})

# Create your views here.
