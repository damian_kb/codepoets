from django.apps import AppConfig


class BenfordLawConfig(AppConfig):
    name = 'benford_law'
