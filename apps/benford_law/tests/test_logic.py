import csv
import os
from unittest import TestCase

from apps.benford_law.logic import calculate_probabilities, generate_benford_probabilities, test_chi_square_for_benford, \
    extract_data_set_from_file, check_file_conform_to_data_format


BASE_TESTS_PATH = os.path.dirname(os.path.realpath(__file__))

class TestLogicFunctions(TestCase):
    def test_calculate_probabilities(self):
        percentages = [10, 30, 60]
        test_data_set = []
        for idx, p in enumerate(percentages):
            test_data_set += [idx] * p
        result = calculate_probabilities(test_data_set)
        self.assertListEqual(result, [p / 100 for p in percentages])

    def test_generate_benford_probabilities_and_chi_square(self):
        digits = range(1, 10)
        data_size = 100
        probabilities = generate_benford_probabilities()
        data_set = []
        for digit, probability in zip(digits, probabilities):
            data_set += [digit] * int(data_size * probability)

        result = test_chi_square_for_benford(data_set)
        self.assertSequenceEqual(result, [True, True])

        probs = list(reversed(data_set))
        result = test_chi_square_for_benford(probs)
        self.assertSequenceEqual(result, [False, False])

    def test_detect_malformed_data_set(self):

        with self.assertRaises(ValueError):
            with open(os.path.join(BASE_TESTS_PATH,'malformed_data_set.csv'), 'r') as f:
                data_set = check_file_conform_to_data_format(f)

        with open(os.path.join(BASE_TESTS_PATH,'test_data_set.csv'), 'r') as f1:
            data_set = check_file_conform_to_data_format(f1)
