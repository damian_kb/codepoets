from django.forms import ModelForm

from apps.benford_law.logic import check_file_conform_to_data_format
from apps.benford_law.models import DataSet


class DataSetForm(ModelForm):
    class Meta:
        model = DataSet
        fields = ['file']

    def is_valid(self):
        try:
            file = self.files['file']
            check_file_conform_to_data_format(file,decode=True)
        except ValueError as e:
            self.errors['file'] = str(e)
            return False
        return True
